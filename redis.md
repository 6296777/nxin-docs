# Redis 缓存类结构
 **1. RedisConnectionHelp**   : 获取/创建ConnectionMultiplexer 连接对象  
    

 **2.RedisHelper.cs**: 对redis操作的封装, 集成序列化/反序列化/自定义key前缀/redis 字符串操作等
 (目前只封装了字符串操作,后续可根据需要添加 list/set/zset/hset 相关操作)    

 **3.RedisCacheHelper cs**:用于将redis作为缓存操作的封装 ,所有方法全是静态,
 目前包括Get/Get/Exists/Remove 操作,使用时直接调用对应方法即可  

# Redis 服务配置(windows下本地开发环境)
1. [Redis 客户端(点击下载)](http://github.com/MSOpenTech/redis/releases):
微软开源团队维护的windows客户端(选择zip)  (Redis没有官方windows版本 windows版本由微软开源团队维护)  
  
2.使用:  

2.1 简单方式:(直接运行服务端,需要手动启动,必须保证窗口不关闭,关闭则redis关闭)   
打开下载的压缩包,解压,双击运行redis-server.exe  ![](http://git.oschina.net/6296777/nxin-docs/raw/develop/references/1.png)  
看到如下图所示时:![](http://git.oschina.net/6296777/nxin-docs/raw/develop/references/2.png) redis 已经启动  
如果没有改动配置文件,此时默认配置为:ip为127.0.0.1 密码为空 端口后6379 (如需修改可参照自带的文档修改)  
  
2.2 配置成服务(开机自启动,挂了自动重启,不需要保持前台窗口活动 **推荐**)  
同样先下载再解压  
进入到redis所在文件夹,然后按住shift,右键文件夹空白处,选择在此处打开命令行,如下图:![](http://git.oschina.net/6296777/nxin-docs/raw/develop/references/3.png)  
在命令行中输入如下命令redis-server --service-install redis.windows.conf 回车,
可能会出现提示框,点击确认即可,成功后  如下图:![](http://git.oschina.net/6296777/nxin-docs/raw/develop/references/4.png)  
在系统服务中也可以查看:![](http://git.oschina.net/6296777/nxin-docs/raw/develop/references/5.png)   
首次使用时,需手工启动服务,右键Redis 点击启动即可